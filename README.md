# Liven Store
Projeto para teste de processo seletivo da Liven.

Tela de produtos + Carrinho de compras. [Demo](https://liven-store-prd.web.app/).

## Instalação
Para instalar, basta rodar o comando `yarn`.

Rode local com o comando `yarn start`.

Se preferir pode usar o `npm install` + `npm start`

## Organização do projeto
Esse projeto foi criado utilizando o comando `npx create-react-app my-app --template redux`, portanto é baseado no [template React + Redux](https://github.com/reduxjs/cra-template-redux). Foi utilizado o padrão de organização desse template.

## Deploy
Foi configurado o deploy automatizado (ou Continuous Deployment) no Gitlab, a partir do arquivo `.gitlab-ci.yml`. Esse arquivo possui uma série de comandos que realiza a implantação no [Firebase Hosting](https://firebase.google.com/docs/hosting?hl=pt-br).

A rotina de deploy só acontece caso seja feito um push ou merge no branch `master`.