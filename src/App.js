import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

import { Navbar } from './features/navbar/Navbar'
import { Home } from './pages/Home'
import { CartPage } from './pages/Cart'

import './App.css'
import 'bulma/css/bulma.css'

function App() {
  return (
    <Router>
      <Navbar/>

      <Switch>
        <Route path="/cart">
          <CartPage />
        </Route>
        <Route path="/">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}

export default App
