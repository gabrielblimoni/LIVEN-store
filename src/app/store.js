import { configureStore } from '@reduxjs/toolkit'
import cartReducer from '../features/cart/cartSlice'
import { loadState } from './localStorage'

export default configureStore({
  reducer: {
    cart: cartReducer,
  },
  preloadedState: loadState(),
})
