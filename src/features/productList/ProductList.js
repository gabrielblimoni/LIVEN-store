import React, { useState, useEffect } from 'react'
import './ProductList.css'

import productService from '../../services/products'
import { useDispatch } from 'react-redux'
import {
    addProductToCart,
} from '../cart/cartSlice'
import Swal from 'sweetalert2'

export function ProductList() {
    const dispatch = useDispatch()

    /**
     * State properties
     */
    const [products, setProducts] = useState([])
    const [isLoadingProducts, setIsLoadingProducts] = useState(true)
    const [currentPage, setCurrentPage] = useState(0)
    const [totalPages, setTotalPages] = useState(1)
    const [productPerPage] = useState(8)

    const handleAddProductToCart = product => {
        dispatch(addProductToCart(product))
        Swal.fire({ 
            title: 'Produto adicionado ao carrinho!',
            timer: 1000,
        })
    }

    /**
     * Fetch all products once the component is created
     */
    useEffect(() => {
        const loadProducts = async () => {
            const products = await productService.list()

            setProducts(products)
            setIsLoadingProducts(false)
        }
        loadProducts()
    }, [])

    /**
     * Calculates total pages ever time products 
     * or productPerPage state variables change
     */
    useEffect(() => {
        function calculateTotalPages () {
            const calcTotalPages = (products.length / productPerPage)
            setTotalPages(Math.ceil(calcTotalPages))
        }
        calculateTotalPages()
    }, [products, productPerPage])

    /* Main component.
     * Check if it is loading products then render a spinner, 
     * else render the products list
     */
    function ProductList () {
        if (isLoadingProducts) {
            return (
                <div className="has-text-centered">
                    <i className="fas fa-spinner fa-pulse fa-4x"></i>
                </div>
            )
        } else {
            return (
                <>
                    <div className="product-list columns is-multiline is-centered">
                        <ProductItemList />
                    </div>
                    <Pagination />
                </>
            )
        }
    }

    /**
     * Render the current page product items
     */
    function ProductItemList () {
        const currentPageProducts = products.slice(
            currentPage * productPerPage, 
            (currentPage + 1) * productPerPage
        )
        return currentPageProducts.map(product =>
            <div className="column is-3" key={product.id}>
                <ProductItem product={product} />
            </div>
        )
    }

    /**
     * Product Item per se. Receives a 'product' prop
     */
    function ProductItem ({ product }) {
        return (
            <div className="card is-clickable product-item">
                <div className="card-image">
                    <figure className="image is-4by3">
                        <img src={product.image} alt={product.name} />
                    </figure>
                </div>
                <div className="card-content">
                    <div className="media">
                        <div className="media-content">
                            <p className="title is-5">{product.name}</p>
                            <p className="subtitle is-6">R$ {product.price}</p>
                        </div>
                    </div>
                </div>
                <footer className="card-footer">
                    {/* eslint-disable-next-line */}
                    <a 
                        className="card-footer-item"
                        onClick={() => handleAddProductToCart(product)}
                    >
                        <i className="fas fa-plus"></i>
                        &nbsp;
                        Carrinho
                    </a>
                </footer>
            </div>
        )
    }

    /**
     * Render the pagination footer component
     */
    function Pagination () {
        return (
            <nav className="pagination is-medium" role="navigation" aria-label="pagination">
                <ul className="pagination-list">
                    <PaginationItems />
                </ul>
            </nav>
        )
    }

    /**
     * Generates each pagination item
     */
    function PaginationItems () {
        const paginationItems = []
        for (let i = 0; i < totalPages; i++) {
            const paginationItem = (
                <li key={i}>
                    {/* eslint-disable-next-line */}
                    <a 
                        onClick={() => setCurrentPage(i)}
                        className={"pagination-link" + (i === currentPage ? ' is-current' : '')}
                    >
                        {i + 1}
                    </a>
                </li>
            )
            paginationItems.push(paginationItem)
        }
        return paginationItems
    }

    return (
        <ProductList />
    );
}
