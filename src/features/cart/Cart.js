import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  incrementProduct,
  decrementProduct,
  selectProducts,
  selectTotalValue,
} from './cartSlice'
import './Cart.css'
import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'

export function Cart() {
  const dispatch = useDispatch()
  const products = useSelector(selectProducts)
  const totalValue = useSelector(selectTotalValue)

  const handleDecrementProduct = product => {
    dispatch(decrementProduct(product))
  }
  const handleIncrementProduct = product => {
    dispatch(incrementProduct(product))
  }

  const handleCloseOrder = () => {
    Swal.fire({
      title: 'Funcionalidade não implementada!',
      icon: 'error',
    })
  }

  return (
    <div className="cart is-unselectable">
      <div className="columns is-centered">
        <div className="column is-8">
          <div className="box">
            <h1 className="title">Carrinho</h1>
            <CartProductList />
            <CheckoutData />
          </div>
        </div>
      </div>
    </div>
  )

  function CartProductList () {
    if (products.length === 0) {
      return (
        <div className="notification is-info">
          Sem produtos no carrinho :(
        </div>
      )
    } else {
      return (
        <ul className="cart-product-list">
          <CartProductItems />
        </ul>
      )
    }
  }

  function CartProductItems () {
    const productItems = []

    products.forEach(product => {
      const productItem = buildProductItem(product)
      productItems.push(productItem)
    })

    return productItems
  }

  function buildProductItem (product) {
    return (
      <li className="cart-product-item" key={product.id}>
        <article className="media">
          <figure className="media-left">
            <p className="image is-64x64">
              <img src={product.image} alt={product.name} />
            </p>
          </figure>
          <div className="media-content">
            <div className="content">
              <p>
                <strong>{product.name}</strong> x {product.quantity}
                <br />
                R$ {product.price} <strong></strong>
              </p>
            </div>
          </div>
          <div className="media-right">
            <div>
              <div onClick={() => handleIncrementProduct(product)}>
                <i className="fas fa-plus is-clickable add-or-remove-item-icon has-text-info"></i>
              </div>
              <div onClick={() => handleDecrementProduct(product)}>
                <i className="fas fa-minus is-clickable add-or-remove-item-icon has-text-danger"></i>
              </div>
            </div>
          </div>
        </article>
      </li>
    )
  }

  function CheckoutData () {
    if (products.length === 0) return (
      <Link to="/">
        <i className="fas fa-backward"></i> Voltar para produtos
      </Link>
    )
    else return (
      <nav className="level">
        <div className="level-left">
          <div className="level-item">
            <p className="subtitle is-5">
              Total: <strong>R$ {totalValue.toFixed(2)}</strong>
            </p>
          </div>
        </div>

        <div className="level-right">
          <p className="level-item">
            <button className="button is-success" onClick={handleCloseOrder}>
              Fechar pedido
            </button>
          </p>
        </div>
      </nav>
    )
  }
}
