import { createSlice } from '@reduxjs/toolkit'

export const cartSlice = createSlice({
  name: 'cart',
  initialState: {
    products: [],
  },
  reducers: {
    addProductToCart: (state, { payload }) => {
      const existentProduct = state.products.find(p => p.id === payload.id)
      if (existentProduct) existentProduct.quantity++
      else { 
        payload.quantity = 1
        state.products.push(payload)
      }
    },
    incrementProduct: (state, { payload }) => {
      const product = state.products.find(product => product.id === payload.id)
      product.quantity++
    },
    decrementProduct: (state, { payload }) => {
      const product = state.products.find(product => product.id === payload.id)
      if (product.quantity > 1) product.quantity--
      else {
        const productIndex = state.products.map(p => p.id).indexOf(payload.id)
        state.products.splice(productIndex, 1)
      }
    },
  },
})

export const { incrementProduct, decrementProduct, addProductToCart } = cartSlice.actions;

export const selectProducts = state => state.cart.products
export const selectTotalValue = state => state.cart.products
  .map(product => product.price * product.quantity)
  .reduce((accumulate, current) => {
    return accumulate + current
  }, 0)
export const selectCountProducts = state => state.cart.products
  .map(product => product.quantity)
  .reduce((accumulate, current) => {
    return accumulate + current
  }, 0)

export default cartSlice.reducer
