import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom"
import { useSelector } from 'react-redux'
import {
    selectCountProducts,
} from '../cart/cartSlice'
import './Navbar.css'

export function Navbar() {
    const productInCartCount = useSelector(selectCountProducts)
    const [isBurguerActive, setIsBurguerActive] = useState(false)
    const [burguerActiveClass, setBurguerActiveClass] = useState('')
    
    const toggleNavbar = () => setIsBurguerActive(!isBurguerActive)

    useEffect(() => {
        const burguerActiveClassName = isBurguerActive ? ' is-active' : ''
        setBurguerActiveClass(burguerActiveClassName)
    }, [isBurguerActive])

    return (
        <nav className="navbar is-fixed-top is-light" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
                <Link to="/" className="navbar-item">
                    <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28" alt="" />
                </Link>

                {/* eslint-disable-next-line */}
                <a 
                    role="button" 
                    className={"navbar-burger burger" + burguerActiveClass} 
                    onClick={toggleNavbar}
                >
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div 
                className={"navbar-menu" + burguerActiveClass}
            >
                <div className="navbar-start">
                    <Link to="/" className="navbar-item" onClick={() => setIsBurguerActive(false)}>
                        Produtos
                    </Link>
                    <Link to="/cart" className="navbar-item" onClick={() => setIsBurguerActive(false)}>
                        <span className="cart-menu-wrapper">
                            <i className="fas fa-shopping-cart"></i>
                            <CartProductCount />
                        </span>
                    </Link>
                </div>
            </div>
        </nav>
    )

    function CartProductCount () {
        if (productInCartCount === 0) return ""
        return (
            <span className="cart-product-count has-background-info has-text-white">
                {productInCartCount}
            </span>
        )
    }
}
