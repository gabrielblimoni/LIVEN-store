import React from 'react'
import { ProductList } from '../features/productList/ProductList'

export function Home () {
    return (
        <section className="section">
            <div className="container mt-3">
                <h1 className="title">Produtos</h1>
                <ProductList />
            </div>
        </section>
    )
}