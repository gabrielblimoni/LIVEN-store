import React from 'react'
import { Cart } from '../features/cart/Cart'

export function CartPage () {
    return (
        <section className="section">
            <div className="container mt-3">
                <Cart />
            </div>
        </section>
    )
}